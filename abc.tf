resource "turbot_smart_folder" "encryption" {
  title          = "Encryption @ ACME"
  description    = "Enforce encryption on a range of resource types per ACME policies."
  parent         = "tmod:@turbot/turbot#/"
}